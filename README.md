# Assignment 2 - Agile Software Practice.
Name: Yiyuan Gao
## Client UI.
- User register
>>Allows the user register
![](./img/register.png)
- User login 
 >>Allows the user login
 ![](./img/login.png)
- Home page
 >>Home page
 ![](./img/home.png)
- Change password
 >>Allows the user change password
 ![](./img/change.png)
- User sign out
 >>Allows the user login out and delete cookies
 ![](./img/signout.png)
- User product list
 >>Allows the user search, sort, add to cart list
 ![](./img/productlist.png)
- User cart list
 >>Allows the user edit cart list, delete
 ![](./img/cartlist.png)
- Admin product list
 >>Allows the admin sort, search, delete, edit product
 ![](./img/productlistadmin.png)
- Admin add product
 >>Allows the admin add product
 ![](./img/add.png)
- Map
 >>Allows the user find map maker
 ![](./img/map.png)
- Video
 >>Allows the user watch youtube video
 ![](./img/video.png)
## Web API CI.
https://gyy01467.gitlab.io/flowershop-master/coverage/lcov-report/
